import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material';

import { ComponentAComponent } from './component-a/component-a.component';
import { ComponentBComponent } from './component-b/component-b.component';
import { SampleOneComponent } from './sample-one.component';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [SampleOneComponent, ComponentAComponent, ComponentBComponent]
})
export class SampleOneModule {}
