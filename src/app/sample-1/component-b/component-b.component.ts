import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ComponentAComponent } from '../component-a/component-a.component';

@Component({
  selector: 'pgs-component-b',
  template: `
    <mat-card>

      <mat-card-header>
        <mat-card-title>Component B</mat-card-title>
      </mat-card-header>

      <mat-card-content>
        @Input() text: <strong>{{text}}</strong>
      </mat-card-content>

    </mat-card>
  `
})
export class ComponentBComponent implements OnInit, AfterViewInit {
  @Input() text: string;

  constructor(private parent: ComponentAComponent) {}

  ngOnInit() {
    // this.parent.text = 'Updated text on NgInit';
    // debugger;

    // this.parent.name = 'Updated name on NgInit';
    // debugger;
  }

  ngAfterViewInit() {
    // this.parent.name = 'Updated name on ngAfterViewInit';
    // debugger;
  }
}
