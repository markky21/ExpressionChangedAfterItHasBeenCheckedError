import {AfterViewInit, Component, OnInit} from '@angular/core';

@Component({
  selector: 'pgs-component-a',
  template: `
    <mat-card>

      <mat-card-header>
        <mat-card-title>Component A</mat-card-title>
      </mat-card-header>

      <mat-card-content>
        public name: <strong>{{name}}</strong><br>
        public desc: <strong>{{desc}}</strong>
      </mat-card-content>

    </mat-card>

    <pgs-component-b [text]="text"></pgs-component-b>
  `
})
export class ComponentAComponent implements OnInit, AfterViewInit {
  public name = 'I am A component';
  public desc = 'A parent of component B';
  public text = 'BComp! I\'m your father!';

  ngOnInit() {
    // debugger;
  }

  ngAfterViewInit() {
    // debugger;
  }
}
