import { Component } from '@angular/core';

@Component({
  selector: 'pgs-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {
  public title = 'Expression Changed After It Has Been Checked Error';
}

