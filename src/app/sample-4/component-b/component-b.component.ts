import { Component } from '@angular/core';

@Component({
  selector: 'pgs-component-b',
  template: `
    <mat-card>

      <mat-card-header>
        <mat-card-title>Component B</mat-card-title>
      </mat-card-header>

      <mat-card-content>
       Text: <strong>{{text}}</strong>
      </mat-card-content>

    </mat-card>
  `
})
export class ComponentBComponent {
  public text = 'I am B component';
}
