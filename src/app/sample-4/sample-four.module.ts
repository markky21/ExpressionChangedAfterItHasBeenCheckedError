import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material';

import { ComponentAComponent } from './component-a/component-a.component';
import { ComponentBComponent } from './component-b/component-b.component';
import { SampleFourComponent } from './sample-four.component';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [SampleFourComponent, ComponentAComponent, ComponentBComponent],
  entryComponents: [ComponentBComponent]
})
export class SampleFourModule {}
