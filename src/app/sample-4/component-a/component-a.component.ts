import { AfterViewInit, Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentBComponent } from '../component-b/component-b.component';

@Component({
  selector: 'pgs-component-a',
  template: `
    <mat-card>

      <mat-card-header>
        <mat-card-title>Component A</mat-card-title>
      </mat-card-header>

      <mat-card-content>
        public name: <strong>{{name}}</strong><br>
        public desc: <strong>{{desc}}</strong>
      </mat-card-content>

    </mat-card>

    <ng-container #vc></ng-container>
  `
})
export class ComponentAComponent implements OnInit, AfterViewInit {
  @ViewChild('vc', { read: ViewContainerRef })
  public vc;

  public name = 'I am A component';
  public desc = 'A parent of component B';

  constructor(private r: ComponentFactoryResolver) {}

  ngOnInit() {
/*  // SOLUTION
    const f = this.r.resolveComponentFactory(ComponentBComponent);
    this.vc.createComponent(f);*/
  }

  ngAfterViewInit() {
    const f = this.r.resolveComponentFactory(ComponentBComponent);
    this.vc.createComponent(f);
  }
}
