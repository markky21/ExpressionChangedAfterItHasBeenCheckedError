import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'pgs-component-b',
  template: `
    <mat-card>

      <mat-card-header>
        <mat-card-title>Component B</mat-card-title>
      </mat-card-header>

      <mat-card-content>
        @Input() text: <strong>{{text}}</strong>
      </mat-card-content>

    </mat-card>
  `
})
export class ComponentBComponent implements OnInit {
  @Input() text: string;
  @Output() newText = new EventEmitter<string>();

  /* // SOLUTION
  @Output() newText = new EventEmitter<string>(true);
  */

  ngOnInit() {
    this.newText.emit('Updated name on ngInit by @Output');
  }
}
