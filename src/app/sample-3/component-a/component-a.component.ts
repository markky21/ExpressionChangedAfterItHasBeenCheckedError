import { Component } from '@angular/core';

@Component({
  selector: 'pgs-component-a',
  template: `
    <mat-card>

      <mat-card-header>
        <mat-card-title>Component A</mat-card-title>
      </mat-card-header>

      <mat-card-content>
        public name: <strong>{{name}}</strong><br>
        public desc: <strong>{{desc}}</strong>
      </mat-card-content>

    </mat-card>

    <pgs-component-b
      (newText)="updateText($event)"
      [text]="text"
    ></pgs-component-b>
  `
})
export class ComponentAComponent {
  public name = 'I am A component';
  public desc = 'A parent of component B';
  public text = 'A message for the child component';

  public updateText(newText: string): void {
    this.text = newText;
  }
}
