import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material';

import { ComponentAComponent } from './component-a/component-a.component';
import { ComponentBComponent } from './component-b/component-b.component';
import { SampleThreeComponent } from './sample-three.component';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [SampleThreeComponent, ComponentAComponent, ComponentBComponent]
})
export class SampleThreeModule {}
