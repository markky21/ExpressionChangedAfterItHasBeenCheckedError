import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './home-page/home-page.component';
import { SampleOneComponent } from './sample-1/sample-one.component';
import { SampleOneModule } from './sample-1/sample-one.module';
import { SampleThreeComponent } from './sample-3/sample-three.component';
import { SampleThreeModule } from './sample-3/sample-three.module';
import { SampleTwoComponent } from './sample-2/sample-two.component';
import { SampleTwoModule } from './sample-2/sample-two.module';
import { SampleFourModule } from './sample-4/sample-four.module';
import { SampleFourComponent } from './sample-4/sample-four.component';
import { SampleFiveComponent } from './sample-5/sample-five.component';
import { SampleFiveModule } from './sample-5/sample-five.module';

const appRoutes: Routes = [
  { path: 'sample-one', component: SampleOneComponent },
  { path: 'sample-two', component: SampleTwoComponent },
  { path: 'sample-three', component: SampleThreeComponent },
  { path: 'sample-four', component: SampleFourComponent },
  { path: 'sample-five', component: SampleFiveComponent },
  { path: '', component: HomePageComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
    SampleOneModule,
    SampleTwoModule,
    SampleThreeModule,
    SampleFourModule,
    SampleFiveModule
  ],
  exports: [RouterModule, SampleOneModule, SampleTwoModule, SampleThreeModule, SampleFourModule, SampleFiveModule]
})
export class AppRouting {}
