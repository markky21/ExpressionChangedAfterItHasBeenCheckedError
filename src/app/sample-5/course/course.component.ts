import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { startWith, tap, delay } from 'rxjs/operators';
import { merge } from 'rxjs';
import { MatPaginator, MatSort } from '@angular/material';

import { CoursesService } from './courses.service';
import { LessonsDataSource } from './lessons.datasource';

@Component({
  selector: 'pgs-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  public dataSource: LessonsDataSource;
  public displayedColumns = ['seqNo', 'description', 'duration'];

  constructor(private coursesService: CoursesService) {}

  ngOnInit() {
    this.dataSource = new LessonsDataSource(this.coursesService);
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith(null),
        // delay(0),
        tap(() => this.loadLessonsPage())
      )
      .subscribe();
  }

  loadLessonsPage() {
    this.dataSource.loadLessons(this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
  }
}
