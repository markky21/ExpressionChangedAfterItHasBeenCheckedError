import { Observable } from 'rxjs';
import { delay } from 'rxjs/internal/operators';

import { LessonInterface } from './lesson.interface';
import { coursesData } from './courses.data';

export class CoursesService {

  public findLessons(sortOrder = 'asc', pageNumber = 0, pageSize = 3): Observable<LessonInterface[]> {
    return Observable.create(observer => {
      observer.next(this.sliceLessons(sortOrder, pageNumber, pageSize));
    }).pipe(delay(1000));
  }

  private sliceLessons(sortOrder: string, pageNumber: number, pageSize: number): LessonInterface[] {
    const courses = [...coursesData];

    sortOrder === 'asc' ? courses.sort((a, b) => a.seqNo - b.seqNo) : courses.sort((a, b) => b.seqNo - a.seqNo);

    return courses.slice(pageNumber * pageSize, pageNumber * pageSize + pageSize);
  }
}
