export interface LessonInterface {
  id: number;
  description: string;
  duration: string;
  seqNo: number;
  courseId: number;
}
