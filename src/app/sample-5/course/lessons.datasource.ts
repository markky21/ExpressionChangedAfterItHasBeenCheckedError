import { BehaviorSubject, Observable } from 'rxjs';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';

import { CoursesService } from './courses.service';
import { LessonInterface } from './lesson.interface';

export class LessonsDataSource implements DataSource<LessonInterface> {

  private lessonsSubject = new BehaviorSubject<LessonInterface[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private coursesService: CoursesService) {}

  loadLessons(sortDirection: string, pageIndex: number, pageSize: number) {
    this.loadingSubject.next(true);

    this.coursesService.findLessons(sortDirection, pageIndex, pageSize).subscribe(lessons => {
      this.loadingSubject.next(false);
      this.lessonsSubject.next(lessons);
    });
  }

  connect(collectionViewer: CollectionViewer): Observable<LessonInterface[]> {
    return this.lessonsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.lessonsSubject.complete();
    this.loadingSubject.complete();
  }
}
