import { Observable, Subject } from 'rxjs';

export class SharedService {
  private subjectText = new Subject<string>();

  public getText(): Observable<string> {
    return this.subjectText;
  }

  public set text(value: string) {
    this.subjectText.next(value);

    /* // SOLUTION
    setTimeout(() => {
      this.subjectText.next(value);
    });
    */
  }
}
