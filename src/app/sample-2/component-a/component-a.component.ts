import { Component, OnDestroy, OnInit } from '@angular/core';
import { delay } from 'rxjs/internal/operators';
import { Subscription } from 'rxjs';

import { SharedService } from '../shared.service';

@Component({
  selector: 'pgs-component-a',
  template: `
    <mat-card>

      <mat-card-header>
        <mat-card-title>Component A</mat-card-title>
      </mat-card-header>

      <mat-card-content>
        public name: <strong>{{name}}</strong><br>
        public desc: <strong>{{desc}}</strong>
      </mat-card-content>

    </mat-card>

    <pgs-component-b [text]="text"></pgs-component-b>
  `
})
export class ComponentAComponent implements OnInit, OnDestroy {
  public name = 'I am A component';
  public desc = 'A parent of component B';
  public text = 'A message for the child component';

  private subscriptions: Array<Subscription> = [];

  constructor(private service: SharedService) {}

  ngOnInit() {
    this.subscriptions.push(
      this.service
        .getText()
        .subscribe(value => {
          this.text = value;
        })
    );

    /* // SOLUTION
   this.subscriptions.push(
      this.service
        .getText()
        .pipe(delay(0))
        .subscribe(value => {
          this.text = value;
        })
    );
    */
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
