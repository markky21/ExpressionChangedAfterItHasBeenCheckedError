import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material';

import { ComponentAComponent } from './component-a/component-a.component';
import { ComponentBComponent } from './component-b/component-b.component';
import { SampleTwoComponent } from './sample-two.component';
import { SharedService } from './shared.service';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [SampleTwoComponent, ComponentAComponent, ComponentBComponent],
  providers: [SharedService]
})
export class SampleTwoModule {}
