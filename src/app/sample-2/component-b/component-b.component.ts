import { Component, Input, OnInit } from '@angular/core';

import { SharedService } from '../shared.service';

@Component({
  selector: 'pgs-component-b',
  template: `
    <mat-card>

      <mat-card-header>
        <mat-card-title>Component B</mat-card-title>
      </mat-card-header>

      <mat-card-content>
        @Input() text: <strong>{{text}}</strong>
      </mat-card-content>

    </mat-card>
  `
})
export class ComponentBComponent implements OnInit {
  @Input() text: string;

  constructor(private service: SharedService) {}

  ngOnInit() {
    // debugger;

    this.service.text = 'Updated name on ngInit by Service';
  }
}
